// Flutter imports:
import 'package:flutter/material.dart';

ScreenSize fromSize(Size size) {
  if (size.width >= AppFormFactor.medium) return ScreenSize.expanded;
  if (size.width >= AppFormFactor.compact) return ScreenSize.medium;

  return ScreenSize.compact;
}

ScreenSize getScreenSize(BuildContext context) {
  final deviceWidth = MediaQuery.of(context).size.width;
  if (deviceWidth >= AppFormFactor.expanded) return ScreenSize.expanded;
  if (deviceWidth >= AppFormFactor.medium) return ScreenSize.medium;
  if (deviceWidth >= AppFormFactor.compact) return ScreenSize.compact;
  return ScreenSize.compact;
}

class AppFormFactor {
  AppFormFactor._();

  static const appBarSize = Size.fromHeight(kToolbarHeight);
  static const double compact = 300;
  static const double expanded = 840;
  static const double medium = 600;
  static const double small = 412;

  static TextScaler textScaler(ScreenSize screenSize) {
    if (screenSize == ScreenSize.medium) {
      return const TextScaler.linear(1.1);
    }

    if (screenSize == ScreenSize.expanded) {
      return const TextScaler.linear(1.2);
    }

    return const TextScaler.linear(1.0);
  }
}

enum ScreenSize {
  compact(AppFormFactor.compact),
  small(AppFormFactor.small),
  medium(AppFormFactor.medium),
  expanded(AppFormFactor.expanded);

  const ScreenSize(this.size);

  final double size;
}

extension ScreenSizeExtensions on ScreenSize {
  bool greaterThan(ScreenSize screenSize) {
    return size > screenSize.size;
  }

  bool greaterThanOrEqual(ScreenSize screenSize) {
    return size >= screenSize.size;
  }
}
