import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import 'build_context.extensions.dart';
import 'state/authentication_state.dart';
import 'theme/theme.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({required this.authenticationState, required this.appRouter, super.key});

  // final AppRouter appRouter;
  final AuthenticationState authenticationState;
  final GoRouter appRouter;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AuthenticationState>(
          create: (_) => authenticationState,
        ),
        ChangeNotifierProxyProvider<AuthenticationState, AppTheme>(
          create: (_) => AppTheme.instance(),
          update: (a, b, c) => AppTheme.instance(
            scheme: MaterialTheme.lightScheme().toColorScheme(),
          ),
        ),
      ],
      child: Consumer2<AuthenticationState, AppTheme>(
        builder: (context, state, theme, _) => MaterialApp.router(
          title: 'Two brothers in a van',
          debugShowCheckedModeBanner: false,
          theme: theme.getAppTheme(context),
          routerConfig: appRouter,
        ),
      ),
    );
  }
}

class AppTheme extends ChangeNotifier {
  AppTheme._();
  factory AppTheme.instance({ColorScheme? scheme}) {
    _selectedColorScheme = scheme ?? MaterialTheme.lightScheme().toColorScheme();

    return AppTheme._();
  }

  static ColorScheme _selectedColorScheme = MaterialTheme.lightHighContrastScheme().toColorScheme();

  static ColorScheme get colorScheme => _selectedColorScheme;

  set selectedColorScheme(ColorScheme value) {
    _selectedColorScheme = value;

    notifyListeners();
  }

  ThemeData getAppTheme(BuildContext context) => ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        primaryColor: MaterialTheme.lightScheme().primary,
        colorScheme: colorScheme,
        appBarTheme: AppBarTheme(
          color: MaterialTheme.lightScheme().primary,
          titleTextStyle: context.textTheme.titleLarge?.copyWith(color: MaterialTheme.lightScheme().onPrimary),
          iconTheme: Theme.of(context).iconTheme.copyWith(color: context.colorScheme.onPrimary),
        ),

        useMaterial3: true,
      );
}

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          body: Container(
            constraints: BoxConstraints(
              maxHeight: constraints.maxHeight,
              maxWidth: 420,
            ),
            child: Column(
              children: [
                const Text('Not found'),
                OutlinedButton(onPressed: () => context.go('/'), child: const Text('Home'))
              ],
            ),
          ),
        );
      },
    );
  }
}
