import 'package:flutter/material.dart';

import 'app_widget.dart';
import 'infrastructure/authentication.dart';
import 'infrastructure/local_repository.dart';
import 'routing/app_router.dart';
import 'state/authentication_state.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final authentication = Authentication();
  final authenticationState = AuthenticationState(false, authentication);

  final appRouter = AppRouter.initialize(authenticationState);

  LocalRepository();

  runApp(
    AppWidget(
      authenticationState: authenticationState,
      appRouter: appRouter,
    ),
  );
}
