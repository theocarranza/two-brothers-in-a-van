import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../infrastructure/local_repository.dart';
import '../string.extentions.dart';

/// The second screen in the bottom navigation bar.
class Billing extends StatelessWidget {
  /// Constructs a [Billing] widget.
  const Billing({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Billing'),
      ),
      body: FutureProvider<List<({String? ammount, String? dueDate})>>(
        create: (context) => LocalRepository().loadInvoicesList(),
        initialData: const [],
        builder: (context, child) => Consumer<List<({String? ammount, String? dueDate})>>(
          builder: (context, list, _) {
            return ListView.builder(
              itemCount: list.length,
              itemBuilder: (context, index) {
                final item = list[index];
                return ListTile(
                  title: Text(item.dueDate.toHumanReadable()),
                  subtitle: Text(item.ammount.toNonNullable()),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
