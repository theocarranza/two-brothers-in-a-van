import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../infrastructure/local_repository.dart';
import '../state/authentication_state.dart';

class Logout extends StatelessWidget {
  final LogoutController controller;

  const Logout({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return controller.authenticationState.isAuthenticated
        ? const Center(child: CircularProgressIndicator())
        : const SizedBox.shrink();
  }
}

class LogoutController extends ChangeNotifier {
  final _localRepository = LocalRepository();

  final AuthenticationState authenticationState;

  LogoutController({required this.authenticationState}) {
    call();
  }

  Future<void> call() async {
    final logout = await _localRepository.deleteUserFromLocalStorage();

    await Future.delayed(Durations.long4, () => authenticationState.isAuthenticated = !logout);
  }
}

class LogoutRoute extends GoRouteData {
  static const name = 'LogoutRoute';
  static const path = 'logout';

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return ChangeNotifierProvider<LogoutController>(
      create: (_) => LogoutController(authenticationState: context.read<AuthenticationState>()),
      builder: (context, child) => Consumer<LogoutController>(
        builder: (context, controller, _) => Logout(
          controller: controller,
        ),
      ),
    );
  }
}
