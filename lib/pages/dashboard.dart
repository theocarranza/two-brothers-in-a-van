import 'package:flutter/material.dart';

import '../routing/generate_routes.fn.dart';

/// The first screen in the bottom navigation bar.
class Dashboard extends StatelessWidget {
  /// Constructs a [Dashboard] widget.
  const Dashboard({this.accountType, super.key});
  final String? accountType;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextButton(
          onPressed: () {
            DashboardDetailRoute(label: DashboardRoute.detailPageTitle).push(context);
          },
          child: const Text('View Dashboard details'),
        ),
      ),
    );
  }
}
