import 'package:flutter/material.dart';

import '../routing/generate_routes.fn.dart';

/// The third screen in the bottom navigation bar.
class Profile extends StatelessWidget {
  /// Constructs a [Profile] widget.
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: TextButton(
          onPressed: () {
            ProfileDetailRoute(label: ProfileRoute.detailPageTitle).go(context);
          },
          child: const Text('View Profile details'),
        ),
      ),
    );
  }
}
