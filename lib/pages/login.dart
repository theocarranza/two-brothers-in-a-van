import 'dart:async';

import 'package:faker_dart/faker_dart.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../app_widget.dart';
import '../build_context.extensions.dart';
import '../layout/app_form_factor.dart';
import '../routing/guards/must_not_be_authenticated.guard_fn.dart';
import '../state/authentication_state.dart';
import '../theme/theme.dart';

class Login extends StatelessWidget {
  const Login({super.key, required this.formController});

  final FormController formController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login or whatever'),
        automaticallyImplyLeading: false,
      ),
      body: Center(
        child: Container(
          alignment: Alignment.center,
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height,
            maxWidth: AppFormFactor.small,
          ),
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(vertical: 14),
            child: Form(
              key: formController.formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    width: AppFormFactor.compact,
                    height: 82,
                    child: TextFormField(
                      initialValue: formController.user?.name,
                      onSaved: (newValue) => formController.user = (
                        name: newValue,
                        email: formController.user?.name,
                        password: formController.user?.password,
                      ),
                      validator: (value) => formController.required(value),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        label: Text('Name'),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: AppFormFactor.compact,
                    height: 82,
                    child: TextFormField(
                      initialValue: formController.user?.email,
                      onSaved: (newValue) => formController.user = (
                        email: newValue,
                        name: formController.user?.name,
                        password: formController.user?.password,
                      ),
                      validator: (value) => formController.required(value),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        label: Text('E-mail'),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: AppFormFactor.compact,
                    height: 82,
                    child: TextFormField(
                      initialValue: formController.user?.password,
                      obscureText: true,
                      textCapitalization: TextCapitalization.none,
                      onSaved: (newValue) => formController.user = (
                        name: formController.user?.name,
                        email: formController.user?.email,
                        password: newValue,
                      ),
                      validator: (value) => formController.required(value),
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        label: Text('Senha'),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 52,
                    width: AppFormFactor.compact,
                    child: ElevatedButton.icon(
                      onPressed: () => formController.submit(context),
                      icon: const Icon(Icons.login_outlined),
                      label: const Text('Entrar'),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: context.colorScheme.tertiary,
        onPressed: () {
          final appTheme = context.read<AppTheme>();
          appTheme.selectedColorScheme =
              appTheme.getAppTheme(context).colorScheme == MaterialTheme.darkMediumContrastScheme().toColorScheme()
                  ? appTheme.selectedColorScheme = MaterialTheme.lightScheme().toColorScheme()
                  : appTheme.selectedColorScheme = MaterialTheme.darkMediumContrastScheme().toColorScheme();
        },
        child: const Icon(Icons.dark_mode_outlined),
      ),
    );
  }
}

class LoginRoute extends GoRouteData {
  static const name = 'login';
  static const path = 'login';

  @override
  FutureOr<String?> redirect(BuildContext context, GoRouterState state) {
    return mustNotBeAuthenticatedRouteGuardFn(context, state);
  }

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return ChangeNotifierProvider<FormController>(
      create: (_) => FormController(context.read<AuthenticationState>()),
      builder: (context, child) => Consumer<FormController>(
        builder: (context, formController, child) => Login(formController: formController),
      ),
    );
  }
}

class FormController extends ChangeNotifier {
  final faker = Faker.instance;
  FormController(this.authenticationState) {
    final firstName = faker.name.firstName().toLowerCase();
    final email = faker.internet.email().toLowerCase();
    final password = faker.lorem.word().toLowerCase();

    user = (email: email, name: firstName, password: password);
  }

  final AuthenticationState? authenticationState;
  final formKey = GlobalKey<FormState>();

  bool? _isValid;

  bool? get isValid => _isValid;

  set isValid(bool? value) {
    _isValid = value;
    notifyListeners();
  }

  ({String? name, String? email, String? password})? _user;

  ({String? name, String? email, String? password})? get user => _user;

  set user(({String? name, String? email, String? password})? value) {
    _user = value;

    notifyListeners();
  }

  submit(BuildContext context) async {
    isValid = formKey.currentState?.validate() ?? false;

    if (isValid == true) {
      formKey.currentState?.save();

      final isAuthenticated = await authenticationState?.authenticate(
        (name: user?.name, email: user?.email, password: user?.password),
      );

      if (!context.mounted) throw StateError('FormController.submit');

      switch (isAuthenticated) {
        case (true):
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Is saved')));
        case (false):
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Is saved')));
      }

      // authenticationState.authenticate((name: user?.name, email: user?.email, password: user?.password));
    }
  }

  String? required(String? value) {
    if (value == null || value.isEmpty) {
      return 'Campo obrigatório';
    }

    return null;
  }
}
