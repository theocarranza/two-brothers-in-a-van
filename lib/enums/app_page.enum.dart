import 'package:collection/collection.dart';

enum AppPage {
  dashboard,
  billing,
  profile,
  root,
  unknown;

  const AppPage();

  static AppPage fromName(String? value) {
    if (value == null) return AppPage.unknown;

    final sanitizedValue = value.split('.')[0];
    final appPage = AppPage.values.singleWhereOrNull(
      (element) {
        return element.name.contains(sanitizedValue);
      },
    );

    return appPage ?? AppPage.root;
  }

  String get toName => name.toString();

  static (AppPage, int) indexedPage(AppPage appPage) => (appPage, appPage.index);
  static List<(AppPage, int)> indexedPages() => (AppPage.values.map((appPage) => indexedPage(appPage)).toList());
  static (int,) fetchPageIndex(int idx) => (AppPage.indexedPage(AppPage.values[idx]).$2,);

  static ({int index}) fetchIndex() {
    return switch (AppPage.values) {
      ([AppPage.dashboard]) => (index: 0),
      ([AppPage.billing]) => (index: 1),
      ([AppPage.profile]) => (index: 2),
      (_) => (index: 0),
    };
  }
}
