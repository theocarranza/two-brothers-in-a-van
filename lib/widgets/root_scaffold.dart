import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../app_widget.dart';
import '../enums/account_type.enum.dart';
import '../layout/app_form_factor.dart';
import '../pages/logout.dart';
import '../routing/generate_routes.fn.dart';
import '../string.extentions.dart';
import '../theme/theme.dart';

class RootScaffold extends StatelessWidget {
  const RootScaffold({
    required this.title,
    super.key,
  });

  final String? title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$title'),
        centerTitle: true,
      ),
      body: LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            constraints: BoxConstraints(
              maxHeight: constraints.maxHeight,
              maxWidth: constraints.maxWidth,
            ),
            alignment: Alignment.center,
            child: SizedBox(
              width: AppFormFactor.small,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Spacer(),
                  Text(
                    '$title'.toCamelCase(),
                    style: Theme.of(context)
                        .typography
                        .tall
                        .displaySmall
                        ?.copyWith(color: Theme.of(context).colorScheme.onSurface),
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Card(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        const SizedBox(
                          height: 12,
                        ),
                        ListTile(
                          title: const Text('Business account'),
                          leading: const Icon(Icons.business_center_outlined),
                          trailing: const Icon(Icons.chevron_right_outlined),
                          horizontalTitleGap: 92,
                          onTap: () {
                            final appTheme = context.read<AppTheme>();
                            appTheme.selectedColorScheme = MaterialTheme.darkMediumContrastScheme().toColorScheme();
                            DashboardRoute(title: 'Business Account', accountType: AccountType.business.name.toString())
                                .push(context);
                          },
                        ),
                        const SizedBox(
                          height: 52,
                          width: AppFormFactor.small,
                          child: Divider(endIndent: 12, indent: 12),
                        ),
                        ListTile(
                          title: const Text('Personal account'),
                          leading: const Icon(Icons.account_box_outlined),
                          trailing: const Icon(Icons.chevron_right_outlined),
                          horizontalTitleGap: 92,
                          onTap: () {
                            final appTheme = context.read<AppTheme>();
                            appTheme.selectedColorScheme = MaterialTheme.lightScheme().toColorScheme();
                            const BillingRoute(title: 'Business account').push(context);
                          },
                        ),
                        const SizedBox(
                          height: 12,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: SizedBox(
                          height: 40,
                          width: AppFormFactor.compact,
                          child: OutlinedButton.icon(
                            icon: const Icon(Icons.logout_outlined),
                            onPressed: () async {
                              await LogoutRoute().push(context);
                              // context.read<AuthenticationState>().isAuthenticated = false;
                            },
                            label: const Text('Sair'),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
