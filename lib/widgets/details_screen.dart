import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../layout/app_form_factor.dart';
import '../state/application_state.dart';
import '../string.extentions.dart';

/// The details screen for either the A, B or C screen.
class DetailsScreen extends StatelessWidget {
  /// Constructs a [DetailsScreen].
  const DetailsScreen({
    required this.label,
    super.key,
  });

  /// The label to display in the center of the screen.
  final String? label;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$label'),
      ),
      body: Center(
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(
            horizontal: 12,
            vertical: 30,
          ),
          child: Column(
            children: [
              Text(
                'details for $label'.toCapitalized(),
                style: Theme.of(context).textTheme.headlineMedium,
              ),
              Container(
                alignment: Alignment.center,
                constraints: const BoxConstraints(
                  maxWidth: AppFormFactor.small,
                ),
                padding: const EdgeInsets.symmetric(
                  horizontal: 30,
                  vertical: 12,
                ),
                child: StreamProvider<({String? name, String? email, String? password})?>(
                  create: (_) => context.read<ApplicationState>().fetchUserStream(),
                  initialData: null,
                  builder: (context, child) => Consumer<({String? name, String? email, String? password})?>(
                    builder: (context, user, child) => user == null
                        ? const Center(child: CircularProgressIndicator())
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  const Expanded(child: Text('Name:')),
                                  Expanded(child: Text(user.name ?? 'user not found', textAlign: TextAlign.left))
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  const Expanded(child: Text('E-mail:')),
                                  Expanded(child: Text(user.email ?? 'user not found', textAlign: TextAlign.left))
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  const Expanded(child: Text('Password:')),
                                  Expanded(child: Text(user.password ?? 'user not found', textAlign: TextAlign.left))
                                ],
                              ),
                            ],
                          ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
