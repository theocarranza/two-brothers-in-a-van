import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../pages/logout.dart';
import '../routing/generate_routes.fn.dart';
import '../state/authentication_state.dart';

class ShellDrawer extends StatelessWidget {
  const ShellDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return NavigationDrawer(
      children: [
        ListTile(
          title: const Text('Início'),
          trailing: const Icon(Icons.home_outlined),
          onTap: () => RootRoute().go(context),
        ),
        ListTile(
          title: const Text('Sair'),
          trailing: const Icon(Icons.logout_outlined),
          onTap: () {
            LogoutRoute().go(context);
            context.read<AuthenticationState>().isAuthenticated = false;
          },
        )
      ],
    );
  }
}
