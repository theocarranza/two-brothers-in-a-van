import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../enums/app_page.enum.dart';
import '../routing/generate_routes.fn.dart';
import '../string.extentions.dart';
import 'shell_drawer.dart';

/// Builds the "shell" for the app by building a Scaffold with a
/// BottomNavigationBar, where [child] is placed in the body of the Scaffold.
class ShellScaffold extends StatelessWidget {
  /// Constructs an [ShellScaffold].
  const ShellScaffold({
    super.key,
    required this.title,
    required this.child,
  });

  /// The widget to display in the body of the Scaffold.
  /// In this sample, it is a Navigator.
  final Widget child;

  /// The title to display in the AppBar.
  final String title;

  RouteMatch getLastMatch(BuildContext context) {
    final RouteMatchList currentConfiguration = GoRouter.of(context).routerDelegate.currentConfiguration;
    return currentConfiguration.last;
  }

  void onTapped(int idx, BuildContext context) {
    return switch (idx) {
      (0) => DashboardRoute(title: title).go(context),
      (1) => BillingRoute(title: title).go(context),
      (2) => ProfileRoute(title: title).go(context),
      (_) => ErrorRoute().go(context),
    };
  }

  static int fetchPageIndex(int idx) => AppPage.fetchPageIndex(idx).$1;

  int fetchIndexByUri(BuildContext context) {
    final pageName = getLastMatch(context).route.name;
    final page = AppPage.fromName(pageName);

    return page.index > 2 ? 0 : page.index;

    // return page.index;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: child,
      appBar: AppBar(
        title: Text(title.toNonNullable()),
      ),
      drawer: const ShellDrawer(),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.dashboard_rounded),
            label: 'Dashbad',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Billing',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_box_outlined),
            label: 'Profile',
          ),
        ],
        currentIndex: fetchIndexByUri(context),
        // onTap: (int idx) => onTapped(idx, context),
        onTap: (int idx) => onTapped(idx, context),
      ),
    );
  }
}

class AppBackButton extends StatelessWidget {
  const AppBackButton({
    super.key,
    required this.context,
  });

  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    final GoRoute? topRoute = GoRouterState.of(context).topRoute;
    final RouteMatchList currentConfiguration = GoRouter.of(context).routerDelegate.currentConfiguration;
    final RouteMatch lastMatch = currentConfiguration.last;
    final Uri location = lastMatch is ImperativeRouteMatch ? lastMatch.matches.uri : currentConfiguration.uri;
    final bool canPop = location.pathSegments.length > 1;

    void onBackbuttonPressed() =>
        topRoute == null ? GoRouter.of(context).routerDelegate.pop() : RootRoute().go(context);

    return canPop
        ? BackButton(onPressed: GoRouter.of(context).pop)
        : BackButton(onPressed: () => onBackbuttonPressed());
  }
}
