import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../app_widget.dart';
import '../enums/app_page.enum.dart';
import '../pages/billing.dart';
import '../pages/dashboard.dart';
import '../pages/login.dart';
import '../pages/logout.dart';
import '../pages/profile.dart';
import '../state/application_state.dart';
import '../string.extentions.dart';
import '../widgets/details_screen.dart';
import '../widgets/root_scaffold.dart';
import '../widgets/shell_scaffold.dart';
import 'app_router.dart';

part 'generate_routes.fn.g.dart';

generateRoutes() {
  return $appRoutes;
}

@TypedGoRoute<RootRoute>(
  path: RootRoute.path,
  name: RootRoute.name,
  routes: [
    TypedGoRoute<LogoutRoute>(
      path: LogoutRoute.path,
      name: LogoutRoute.name,
    ),
    TypedGoRoute<LoginRoute>(
      path: LoginRoute.path,
      name: LoginRoute.name,
    ),
    TypedGoRoute<ErrorRoute>(
      path: 'route-error',
      name: 'route_error',
    ),
    TypedShellRoute<HomeShellRoute>(
      routes: [
        TypedGoRoute<DashboardRoute>(
          path: DashboardRoute.path,
          name: DashboardRoute.name,
          routes: [
            TypedGoRoute<DashboardDetailRoute>(
              path: DashboardDetailRoute.path,
              name: DashboardDetailRoute.name,
            )
          ],
        ),
        TypedGoRoute<BillingRoute>(
          path: BillingRoute.path,
          name: BillingRoute.name,
          routes: [
            TypedGoRoute<BillingDetailRoute>(
              path: BillingDetailRoute.path,
              name: BillingDetailRoute.name,
            )
          ],
        ),
        TypedGoRoute<ProfileRoute>(
          path: ProfileRoute.path,
          name: ProfileRoute.name,
          routes: [
            TypedGoRoute<ProfileDetailRoute>(
              path: ProfileDetailRoute.path,
              name: ProfileDetailRoute.name,
            )
          ],
        ),
      ],
    ),
  ],
)
class RootRoute extends GoRouteData {
  RootRoute();

  static final GlobalKey<NavigatorState> $navigatorKey = $rootNavigatorKey;
  static const String name = 'root';
  static const String path = '/';
  static const title = 'Root';

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const RootScaffold(title: 'Root');
  }
}

class HomeShellRoute extends ShellRouteData {
  const HomeShellRoute();

  static final GlobalKey<NavigatorState> $navigatorKey = $shellNavigatorKey;
  static final GlobalKey<NavigatorState> $parentNavigatorKey = RootRoute.$navigatorKey;

  @override
  Page<void> pageBuilder(BuildContext context, GoRouterState state, Widget navigator) {
    return MaterialPage(
      child: ChangeNotifierProvider<ApplicationState>(
        create: (_) => ApplicationState.initialize(),
        builder: (context, _) {
          final String? routeName = GoRouterState.of(context).topRoute?.name;

          (String?,) matchedRouteName(String? value) {
            return (AppPage.fromName(routeName).name,);
          }

          return ShellScaffold(
            title: matchedRouteName(routeName).$1.toNonNullable(),
            child: navigator,
          );
        },
      ),
    );
  }
}

// Dashboard
class DashboardRoute extends GoRouteData {
  const DashboardRoute({required this.title, this.accountType});

  static final GlobalKey<NavigatorState> $parentNavigatorKey = HomeShellRoute.$navigatorKey;
  static const String detailPageTitle = 'dashboard detail';

  static const String name = 'dashboard';
  static const String path = 'dashboard';

  final String? title;
  final String? accountType;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const Dashboard();
  }
}

class DashboardDetailRoute extends GoRouteData {
  DashboardDetailRoute({required this.label});

  static const String name = 'dashboard.detail';
  static const String path = 'dashboard/detail';

  final String? label;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return DetailsScreen(label: label);
  }
}

// Billing
class BillingRoute extends GoRouteData {
  const BillingRoute({required this.title});

  static const String detailPageTitle = 'billing detail';
  static const String name = 'billing';
  static const String path = 'billing';

  final String? title;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const Billing();
  }
}

class BillingDetailRoute extends GoRouteData {
  BillingDetailRoute({required this.label});

  static const String name = 'billing.detail';
  static const String path = 'billing/detail';

  final String? label;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return DetailsScreen(label: label);
  }
}

// Profile

class ProfileRoute extends GoRouteData {
  const ProfileRoute({required this.title});

  static const String detailPageTitle = 'profile detail';
  static const String name = 'profile';
  static const String path = 'profile';

  final String? title;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const Profile();
  }
}

class ProfileDetailRoute extends GoRouteData {
  ProfileDetailRoute({required this.label});

  static const String name = 'profile.detail';
  static const String path = 'profile/detail';

  final String? label;

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return DetailsScreen(label: label);
  }
}

class ErrorRoute extends GoRouteData {
  ErrorRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) {
    return const ErrorScreen();
  }
}
