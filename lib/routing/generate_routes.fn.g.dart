// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'generate_routes.fn.dart';

// **************************************************************************
// GoRouterGenerator
// **************************************************************************

List<RouteBase> get $appRoutes => [
      $rootRoute,
    ];

RouteBase get $rootRoute => GoRouteData.$route(
      path: '/',
      name: 'root',
      factory: $RootRouteExtension._fromState,
      routes: [
        GoRouteData.$route(
          path: 'logout',
          name: 'LogoutRoute',
          factory: $LogoutRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: 'login',
          name: 'login',
          factory: $LoginRouteExtension._fromState,
        ),
        GoRouteData.$route(
          path: 'route-error',
          name: 'route_error',
          factory: $ErrorRouteExtension._fromState,
        ),
        ShellRouteData.$route(
          navigatorKey: HomeShellRoute.$navigatorKey,
          parentNavigatorKey: HomeShellRoute.$parentNavigatorKey,
          factory: $HomeShellRouteExtension._fromState,
          routes: [
            GoRouteData.$route(
              path: 'dashboard',
              name: 'dashboard',
              parentNavigatorKey: DashboardRoute.$parentNavigatorKey,
              factory: $DashboardRouteExtension._fromState,
              routes: [
                GoRouteData.$route(
                  path: 'dashboard/detail',
                  name: 'dashboard.detail',
                  factory: $DashboardDetailRouteExtension._fromState,
                ),
              ],
            ),
            GoRouteData.$route(
              path: 'billing',
              name: 'billing',
              factory: $BillingRouteExtension._fromState,
              routes: [
                GoRouteData.$route(
                  path: 'billing/detail',
                  name: 'billing.detail',
                  factory: $BillingDetailRouteExtension._fromState,
                ),
              ],
            ),
            GoRouteData.$route(
              path: 'profile',
              name: 'profile',
              factory: $ProfileRouteExtension._fromState,
              routes: [
                GoRouteData.$route(
                  path: 'profile/detail',
                  name: 'profile.detail',
                  factory: $ProfileDetailRouteExtension._fromState,
                ),
              ],
            ),
          ],
        ),
      ],
    );

extension $RootRouteExtension on RootRoute {
  static RootRoute _fromState(GoRouterState state) => RootRoute();

  String get location => GoRouteData.$location(
        '/',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $LogoutRouteExtension on LogoutRoute {
  static LogoutRoute _fromState(GoRouterState state) => LogoutRoute();

  String get location => GoRouteData.$location(
        '/logout',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $LoginRouteExtension on LoginRoute {
  static LoginRoute _fromState(GoRouterState state) => LoginRoute();

  String get location => GoRouteData.$location(
        '/login',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $ErrorRouteExtension on ErrorRoute {
  static ErrorRoute _fromState(GoRouterState state) => ErrorRoute();

  String get location => GoRouteData.$location(
        '/route-error',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $HomeShellRouteExtension on HomeShellRoute {
  static HomeShellRoute _fromState(GoRouterState state) =>
      const HomeShellRoute();
}

extension $DashboardRouteExtension on DashboardRoute {
  static DashboardRoute _fromState(GoRouterState state) => DashboardRoute(
        title: state.uri.queryParameters['title'],
        accountType: state.uri.queryParameters['account-type'],
      );

  String get location => GoRouteData.$location(
        '/dashboard',
        queryParams: {
          if (title != null) 'title': title,
          if (accountType != null) 'account-type': accountType,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $DashboardDetailRouteExtension on DashboardDetailRoute {
  static DashboardDetailRoute _fromState(GoRouterState state) =>
      DashboardDetailRoute(
        label: state.uri.queryParameters['label'],
      );

  String get location => GoRouteData.$location(
        '/dashboard/dashboard/detail',
        queryParams: {
          if (label != null) 'label': label,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $BillingRouteExtension on BillingRoute {
  static BillingRoute _fromState(GoRouterState state) => BillingRoute(
        title: state.uri.queryParameters['title'],
      );

  String get location => GoRouteData.$location(
        '/billing',
        queryParams: {
          if (title != null) 'title': title,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $BillingDetailRouteExtension on BillingDetailRoute {
  static BillingDetailRoute _fromState(GoRouterState state) =>
      BillingDetailRoute(
        label: state.uri.queryParameters['label'],
      );

  String get location => GoRouteData.$location(
        '/billing/billing/detail',
        queryParams: {
          if (label != null) 'label': label,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $ProfileRouteExtension on ProfileRoute {
  static ProfileRoute _fromState(GoRouterState state) => ProfileRoute(
        title: state.uri.queryParameters['title'],
      );

  String get location => GoRouteData.$location(
        '/profile',
        queryParams: {
          if (title != null) 'title': title,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

extension $ProfileDetailRouteExtension on ProfileDetailRoute {
  static ProfileDetailRoute _fromState(GoRouterState state) =>
      ProfileDetailRoute(
        label: state.uri.queryParameters['label'],
      );

  String get location => GoRouteData.$location(
        '/profile/profile/detail',
        queryParams: {
          if (label != null) 'label': label,
        },
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}
