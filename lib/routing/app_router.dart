import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../app_widget.dart';
import '../state/authentication_state.dart';
import 'generate_routes.fn.dart';
import 'guards/must_be_authenticated.guard_fn.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'root');
final GlobalKey<NavigatorState> _shellNavigatorKey = GlobalKey<NavigatorState>(debugLabel: 'shell');

GlobalKey<NavigatorState> get $rootNavigatorKey => _rootNavigatorKey;
GlobalKey<NavigatorState> get $shellNavigatorKey => _shellNavigatorKey;

class AppRouter {
  AppRouter(this.authenticationState) : routerConfig = initialize(authenticationState);

  final AuthenticationState authenticationState;
  late final GoRouter routerConfig;

  static GoRouter initialize(AuthenticationState authenticationState) {
    return GoRouter(
      navigatorKey: _rootNavigatorKey,
      initialLocation: '/',
      debugLogDiagnostics: false,
      routes: generateRoutes(),
      errorBuilder: (context, state) => const ErrorScreen(),
      refreshListenable: authenticationState,
      redirect: (context, state) => mustBeAuthenticatedGuardFn(context, state),
    );
  }
}
