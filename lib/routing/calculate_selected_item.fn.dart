import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

int calculateSelectedIndex(BuildContext context) {
  final String location = GoRouterState.of(context).uri.toString();

  if (location.startsWith('/dashboard')) {
    return 0;
  }
  if (location.startsWith('/billing')) {
    return 1;
  }
  if (location.startsWith('/profile')) {
    return 2;
  }
  if (location.endsWith('/')) {
    return 3;
  }

  return 0;
}
