import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../../pages/login.dart';
import '../../state/authentication_state.dart';

FutureOr<String?> mustBeAuthenticatedGuardFn(BuildContext context, GoRouterState state) {
  final isAuthenticated = context.read<AuthenticationState>().isAuthenticated;
  final isLoginRouteMatched = state.matchedLocation == state.namedLocation(LoginRoute.name);

  bool shouldRedirect(bool isOnLoginRoute, bool isAuthenticated) => !isOnLoginRoute && !isAuthenticated;

  if (shouldRedirect(isLoginRouteMatched, isAuthenticated)) {
    return state.namedLocation(LoginRoute.name);
  }

  return null;
}
