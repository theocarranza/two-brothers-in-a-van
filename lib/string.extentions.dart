import 'package:collection/collection.dart';
import 'package:intl/intl.dart';

extension StringExtensions on String? {
  String toCapitalized() {
    return _throwIfNull(this).replaceFirstMapped(RegExp(r'^\w'), (match) => '${match[0]}'.toUpperCase());
  }

  String toCamelCase() {
    return RegExp(r'\w+')
        .allMatches(_throwIfNull(this))
        .map(
          (e) => _throwIfNull(e.group(0)).toCapitalized(),
        )
        .join(' ');
  }

  static String _throwIfNull(String? value) {
    final stringOrThrow = ArgumentError.checkNotNull(value, 'TO_NON_NULLABLE_FAILURE');
    return stringOrThrow.split('').whereNotNull().join();
  }

  String toNonNullable() => _throwIfNull(this);

  String segmentFromUri(String searchString) {
    final segments = Uri.parse(_throwIfNull(this)).pathSegments;
    final matchedSegment = segments.singleWhere((element) => element.contains(searchString));

    return matchedSegment;
  }

  String toHumanReadable() {
    final value = toNonNullable();
    final parsed = DateTime.parse(value);

    final result = DateFormat('d/M/y').format(parsed);

    return result.toString();
  }
}
