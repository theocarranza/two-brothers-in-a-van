import 'package:flutter/material.dart';

import '../infrastructure/authentication.dart';
import '../infrastructure/local_repository.dart';

class AuthenticationState with ChangeNotifier {
  AuthenticationState(this._isAuthenticated, this.authentication) {
    initialize();
  }

  late bool _isAuthenticated;
  final Authentication authentication;

  Future<void> initialize() async {
    _isAuthenticated = await ensureIsAuthenticated();

    notifyListeners();
  }

  bool get isAuthenticated => _isAuthenticated;

  set isAuthenticated(bool value) {
    _isAuthenticated = value;

    notifyListeners();
  }

  authenticate(({String? name, String? email, String? password}) login) async {
    isAuthenticated = await authentication.authenticate(login);

    notifyListeners();
  }

  Future<bool> ensureIsAuthenticated() async {
    final userFromStorage = await LocalRepository().retrieveUserFromLocalStorage();
    if (userFromStorage?.email != null && userFromStorage?.name != null && userFromStorage?.password != null) {
      return true;
    }

    return false;
  }
}
