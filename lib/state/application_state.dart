import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import '../infrastructure/local_repository.dart';

class ApplicationState extends ChangeNotifier {
  ApplicationState._();

  factory ApplicationState.initialize() {
    return ApplicationState._();
  }

  ({String? name, String? email, String? password}) _user = (name: null, email: null, password: null);

  ({String? name, String? email, String? password}) get user => _user;

  set user(({String? name, String? email, String? password}) value) {
    _user = value;

    notifyListeners();
  }

  void initializeState({required ({String? name, String? email, String? password})? value}) {
    if (value != null) {
      user = value;
    }
  }

  ReplayStream<({String? email, String? name, String? password})?> fetchUserStream() {
    final repository = LocalRepository();
    final userSubject = ReplaySubject<({String? email, String? name, String? password})?>();
    userSubject.add((name: null, email: null, password: null));

    final data = repository.retrieveUserFromLocalStorage().asStream();

    data.listen((event) {
      userSubject.sink.add(event);
    });

    return userSubject.stream;
  }
}
