import 'dart:convert';

import 'package:faker_dart/faker_dart.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalRepository extends ChangeNotifier {
// ignore: constant_identifier_names
  static const USER_KEY = 'aeiouAEIOU!';
  SharedPreferences? _sharedPreferences;
  final faker = Faker.instance;

  LocalRepository() {
    _init();
  }

  Future<bool> deleteUserFromLocalStorage() async {
    final preferences = await getPreferences();

    final result = await preferences.remove(USER_KEY);

    return result;
  }

  Map<String, dynamic> generateInvoice(int index) {
    final currentDate = DateTime.now();
    return {
      'dueDate': DateTime(currentDate.year, currentDate.month + index, currentDate.day).toIso8601String(),
      'ammountPayable': faker.commerce.price().toString(),
    };
  }

  List<Map<String, dynamic>> generateInvoices(int length) {
    return List<Map<String, dynamic>>.generate(length, (index) => generateInvoice(index));
  }

  Future<SharedPreferences> getPreferences() async {
    return _sharedPreferences ??= await SharedPreferences.getInstance();
  }

  Future<List<({String? ammount, String? dueDate})>> loadInvoicesList() async {
    final preferences = await getPreferences();

    final List<String>? fromStorage = preferences.getStringList('INVOICES_LIST');

    final jsonList = fromStorage?.map((e) => jsonDecode(e)).toList();

    List<({String? dueDate, String? ammount})>? invoicesList(List<dynamic>? data) {
      return data?.map((e) {
        return (dueDate: e?['dueDate'] as String?, ammount: e?['ammountPayable'] as String?);
      }).toList();
    }

    var invoices = invoicesList(jsonList);

    return invoices ?? [];
  }

  Future<({String? email, String? name, String? password})?> retrieveUserFromLocalStorage() async {
    final preferences = await getPreferences();

    final fromStorage = preferences.getString(USER_KEY);

    final json = fromStorage != null ? jsonDecode(fromStorage) : preferences.getString(USER_KEY);

    ({String? email, String? name, String? password}) userInfo(dynamic data) {
      return (
        name: data?['name'] as String?,
        email: data?['email'] as String?,
        password: data?['password'] as String?,
      );
    }

    var info = userInfo(json);
    var name = info.name;
    var email = info.email;
    var password = info.password;

    return (name: name, email: email, password: password);
  }

  Future<bool> saveUserOnLocalStorage({required ({String? name, String? email, String? password}) user}) async {
    final preferences = await getPreferences();

    final Map<String, dynamic> toMap = {
      'name': user.name,
      'email': user.email,
      'password': user.password,
    };

    final result = await preferences.setString(USER_KEY, jsonEncode(toMap));

    return result;
  }

  Future<bool> storeInvoicesList() async {
    final preferences = await getPreferences();

    final isLoaded = preferences.containsKey('INVOICES_LIST');

    if (isLoaded) {
      return true;
    }

    final result = await preferences.setStringList(
      'INVOICES_LIST',
      generateInvoices(10).map((e) => jsonEncode(e)).toList(),
    );

    return result;
  }

  Future<void> _init() async {
    if (_sharedPreferences != null) {
      return;
    }

    _sharedPreferences = await SharedPreferences.getInstance();

    await storeInvoicesList();
  }
}
