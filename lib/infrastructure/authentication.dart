import 'local_repository.dart';

class Authentication {
  Authentication();
  final repository = LocalRepository();

  Future<bool> authenticate(({String? name, String? email, String? password}) login) async {
    final isUserStored = await repository.saveUserOnLocalStorage(user: login);

    return isUserStored;
  }

  Future<void> logoff() async {
    await repository.deleteUserFromLocalStorage();
  }
}
