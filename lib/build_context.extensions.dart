import 'package:flutter/material.dart';

import 'theme/theme.dart';

extension BuildContextExtensions on BuildContext {
  ColorScheme get colorScheme => Theme.of(this).colorScheme;
  TextTheme get textTheme => MaterialTheme(Theme.of(this).textTheme).textTheme;
}
